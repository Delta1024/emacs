(define-derived-mode blueprint-mode
  org-mode "Blueprint"
  "Major mode for writing novels. Mainly contains templates and org-mode optimizations for writing")

(add-hook 'blueprint-mode-hook 'blueprint-mode-custom-settings)

(defun blueprint-mode-restart () "Restart blueprint-mode" (interactive)
       (org-mode-restart)
       (blueprint-mode))

(defun blueprint-mode-custom-settings () "Sets variables for blueprint mode"
       (display-line-numbers-mode 1)
       (auto-fill-mode 1)
       (org-indent-mode 1)
       (ispell-minor-mode 1))

