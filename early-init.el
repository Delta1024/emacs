;;; early-init.el --- early bird -*- no-byte-compile: t -*-
(setq load-prefer-newer t)
(add-to-list 'load-path "/home/jake/.emacs.d/elpa/packed-20210503.2046")
(add-to-list 'load-path "/home/jake/.emacs.d/elpa/auto-compile-20210615.1457")
(require 'auto-compile)
(auto-compile-on-load-mode)
(auto-compile-on-save-mode)
