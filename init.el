;; (load-file (expand-file-name "README.el" user-emacs-directory))

(org-babel-load-file
 (expand-file-name
  "README.org"
  user-emacs-directory))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auth-source-save-behavior nil)
 '(helm-minibuffer-history-key "M-p")
 '(load-themes '(deeper-blue))
 '(org-src-preserve-indentation t)
 '(package-selected-packages
   '(org-bullets auto-compile typo typopunct lua-mode rustdoc helm-ag helm helm-core popup async quelpa evil-tutor ivy-yasnippet company-lsp vimish-fold electric-pair undo-tree auto-package-update all-the-icons ob-rust counsel-projectile flycheck magit toc-org lsp-treemacs company-emacs-eclim company dashboard rustic lsp-ivy lsp-ui counsel ivy general doom-modeline which-key evil-collection evil use-package))
 '(safe-local-variable-values '((eval org-hide-block-startup 1)))
 '(which-key-mode t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
